#pragma once

#include <Arduino.h>
#include <Wire.h>

// Import hardware/real-world information
#include "MCU_Info.h"  // wirering of MCU

// // Runtime variables //
// // Time of last Tick
// unsigned long long lastMCUTick;

// eInk Display Interface //
#define ENABLE_GxEPD2_GFX 1 // If 1, more ram+flash is used to use the adafruit GFX library
//#include <GFX.h>            // very basic gfx integration
//#include <GxEPD2_BW.h>      // including both doesn't use more code or ram
#include <GxEPD2_3C.h>      // including both doesn't use more code or ram

// #include <Fonts/FreeSansBold9pt7b.h>

// select the display class and display driver class in the following file (new style):
#include "GxEPD2_display_selection_new_style.h"

