#include <Arduino.h>

// // Hardware Definitions ePaper Display
// #define SPI_EPD_CLK     14   // D5
// #define SPI_EPD_MOSI    13   // D7
// #define SPI_EPD_MISO    -1  // not connected
// #define SPI_EPD_CS      15  // D8
// #define SPI_EPD_BUSY    4  // D2
// #define SPI_EPD_RST     2  // D4
// #define SPI_EPD_DC      0   // D3


#define DEBUG
#define DEBUGGPS

// #include <Timezone.h>
#include <TinyGPS++.h>
#include <SoftwareSerial.h>

// // Timezone Settings
// // Change these two rules corresponding to your timezone, see https://github.com/JChristensen/Timezone
// // Central European Time (Frankfurt, Paris)  120 = +2 hours in daylight saving time (summer).
// TimeChangeRule CEST = {"CEST", Last, Sun, Mar, 2, 120};
// // Central European Time (Frankfurt, Paris)  60  = +1 hour in normal time (winter)
// TimeChangeRule CET = {"CET ", Last, Sun, Oct, 3, 60};
// Timezone CE(CEST, CET); // Combine Summer/Wintertime

// // Local Time Object
// time_t localTime;

// // Define function, where the time of MCU is set.
// static void setMCUTime();

// GPS Interface
TinyGPSPlus gps;                                            // the TinyGPS++ object
SoftwareSerial gpsSerial(4, 5); // the serial interface to the GPS device

// // eInk Display Interface //
// #define ENABLE_GxEPD2_GFX 1 // If 1, more ram+flash is used to use the adafruit GFX library
// // #include <GFX.h>            // very basic gfx integration
// //#include <GxEPD2_BW.h>      // including both doesn't use more code or ram
// #include <GxEPD2_3C.h>      // including both doesn't use more code or ram

// // select the display class and display driver class in the following file (new style):
// #include "GxEPD2_display_selection_new_style.h"

int buttonState =0;

void setup() {
  // put your setup code here, to run once:

  Serial.begin(115200);
  Serial.println("start");

  pinMode(12, INPUT);

      // Setup the GPS
    pinMode(2, OUTPUT); // Set the PIN
    digitalWrite(2, LOW); // Switch OFF the GPS

    digitalWrite(2, HIGH); // Switch ON the GPS
    gpsSerial.begin(9600); // Initialize Serial. Pay attention, only works if GPS on


//   // Setup the eInk display.
//   display.init();

//   // Show the Busy Display
//   display.firstPage();
//   do
//   {
//     display.fillScreen(GxEPD_WHITE);
//     display.setTextColor(GxEPD_BLACK);
//     display.setTextSize(2);

//     display.setCursor(20,20);
//     display.print("Lesezeichen");
//     display.setCursor(20, 50);
//     display.print("werden");
//     display.setCursor(20, 80);
//     display.print("gesucht!");

//     display.setCursor(20, 150);
//     display.print("Bitte ");
//     display.setCursor(20, 170);
//     display.print("warten...");

//   } while (display.nextPage());
//   display.hibernate();
}

void loop() {
  // put your main code here, to run repeatedly:
  //Serial.println("loop");

  if (gps.date.year() > 2021)
            {
                // only this can indicate, if the date is more or less updated. tends to be 2020/1970 if old
                #ifdef DEBUG
                    // char sz[32];
                    // sprintf(sz, "%02d.%02d.%02d ", gps.date.day(), gps.date.month(), gps.date.year());
                    Serial.print("Date-year: ");
                    // Serial.println(sz);
                    Serial.println(gps.date.year());
                    
                    // Serial.print("Loc: ");
                    // Serial.print(gps.location.lat());
                    // Serial.print(" ");
                    // Serial.println(gps.location.lng());
                    // Serial.print("Dist m:");

                    // Serial.print("hdop ");
                    // Serial.println(gps.hdop.value());

                    // Serial.print("sats ");
                    // Serial.println(gps.satellites.value());
                #endif

                if ((gps.location.isValid()) & (gps.location.age() < 2000)) // check if gps location is ok and not too old
                {
                    // Calculate the distance to the Target
                    unsigned long distanceMToTarget =1;
                    #ifdef DEBUG
                        Serial.print("Loc: ");
                        Serial.print(gps.location.lat());
                        Serial.print(" ");
                        Serial.println(gps.location.lng());
                        Serial.print("Dist m:");

                        Serial.println(distanceMToTarget);
                        Serial.println("GPS valid");
                        Serial.println(gps.hdop.hdop());
                    #endif
                }
            }

    // Not on tick - Update GPS
    if(gpsSerial.available()) // run only, if GPS is in active state
    {
        if (gps.encode(gpsSerial.read()))
        {
            //setMCUTime(); // Update time
            #ifdef DEBUGGPS
                Serial.println("GPS active"); //See if GPS data is read.
            #endif
        }
    }
}