#pragma once
#include <Arduino.h>
struct MCU_Info {

    // Tick interval
    static constexpr long long MCUTick = 1000; // in ms

    //GPS
    static constexpr int GPSPowerPIN = 2; // MOSFET for turning GPS on/off
    static constexpr int RXPin = 4, TXPin = 5;
    static constexpr uint32_t GPSBaud = 9600; // Default baud of NEO-6M is 9600
    static constexpr long long GPSAbortTime = 300000; // in ms, time until abort activation

    // Button Input
    static constexpr int buttonPIN = 12; // Digital 12 PIN.

    // LOCK CONTROL
    // Define Lock PIN
    static constexpr int lockPIN = 3; // Digital PIN 3, connected to MOSFET ground. Will unlock the lock
    static constexpr int unlockTime = 3000; // Time in ms to unlock the lock

    // EEPROM
    static constexpr int attemptAdress = 20; // Adress of Number Attempts left
    static constexpr int gameStateAdress = 999; // Adress of GameState

    // Waiting periods
    static constexpr int delayStartDisplay = 12000; //us 45s delay (+ display update time) for seeing the info screen
};