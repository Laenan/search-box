#pragma once
#include <Arduino.h>

struct Game_Info {
// GPS Target Information:
static constexpr double TARGET_LAT = 47.025806, 
                        TARGET_LNG = 9.230083; // Current Target: Wissmilenpass

// static constexpr double TARGET_LAT = 47.675853509175,
//                         TARGET_LNG = 9.183740798176322; // Test Target: KN

static constexpr double TARGET_DISTANCE_GOAL_M = 3000; // Distance to target, where Box is opened in meter

// GAME INPUT
static constexpr int numberSolveAttempt = 10; // Number of attempts (with GPS fix)
};