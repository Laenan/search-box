#pragma once

#include <Arduino.h>

// Import hardware/real-world information
#include "MCU_Info.h"  // wirering of MCU
#include "Game_Info.h" // target information for game

// Import for GPS //

// Basic Libraries
// #include <Timezone.h>
#include <TinyGPS++.h>
#include <SoftwareSerial.h>

// // Timezone Settings
// // Change these two rules corresponding to your timezone, see https://github.com/JChristensen/Timezone
// // Central European Time (Frankfurt, Paris)  120 = +2 hours in daylight saving time (summer).
// TimeChangeRule CEST = {"CEST", Last, Sun, Mar, 2, 120};
// // Central European Time (Frankfurt, Paris)  60  = +1 hour in normal time (winter)
// TimeChangeRule CET = {"CET ", Last, Sun, Oct, 3, 60};
// Timezone CE(CEST, CET); // Combine Summer/Wintertime

// // Local Time Object
// time_t localTime;

// // Define function, where the time of MCU is set.
// static void setMCUTime();

// GPS Interface
TinyGPSPlus gps;                                            // the TinyGPS++ object
SoftwareSerial gpsSerial(MCU_Info::RXPin, MCU_Info::TXPin); // the serial interface to the GPS device

// eInk Display Interface //
#define ENABLE_GxEPD2_GFX 0 // If 1, more ram+flash is used to use the adafruit GFX library
#include <GFX.h>            // very basic gfx integration
//#include <GxEPD2_BW.h>      // including both doesn't use more code or ram
#include <GxEPD2_3C.h>      // including both doesn't use more code or ram

// select the display class and display driver class in the following file (new style):
#include "GxEPD2_display_selection_new_style.h"

// Load bitmaps
#include "bitmap_initial-var.h"
//#include "bitmap_busy.h"

// EEPROM //
#include <EEPROM.h>

// Runtime variables //
// Time of last Tick
unsigned long long lastMCUTick;
unsigned long long ButtonPressTick;

// Button pressed switch, to check if start in programm
bool buttonPressed = false;
bool displayFirst = false;
bool gpsPositionFix = false;

// Display functions
void showStartDisplay();
