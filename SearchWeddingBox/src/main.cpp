// Debug Flag, if defined, Serial will output informations
// #define DEBUG
// #define DEBUGGPS
// #define DEBUGDISPLAY
// #define DEBUGFLOW

// Should be commented out for production! Only use once, to reset the EEPROM
//  #define RESETEEPROM

#include "header.h" // Import the header

#include "setup.h" // Import setup function.


void loop()
{    
    if (millis() > lastMCUTick + MCU_Info::MCUTick)
    {
        // On Tick
        
        lastMCUTick = millis();
        #ifdef DEBUGFLOW
            Serial.print("On t: ");
            Serial.println(millis());
        #endif      

        if(buttonPressed & !displayFirst & !gpsPositionFix) // Button was pressed initially, no GPS power
        {
            #ifdef DEBUGFLOW
                Serial.println("L - start");
            #endif
            // Show the Busy Display
            display.firstPage();
            do
            {
                display.fillScreen(GxEPD_WHITE);
                display.setTextColor(GxEPD_BLACK);
                display.setTextSize(2);

                display.setCursor(20,20);
                display.print("Lesezeichen");
                display.setCursor(20, 50);
                display.print("werden");
                display.setCursor(20, 80);
                display.print("gesucht!");

                display.setCursor(20, 150);
                display.print("Bitte ");
                display.setCursor(20, 170);
                display.print("warten...");

            } while (display.nextPage());
            display.hibernate();

            // Switch on the GPS
            displayFirst = true;

        }

        if(buttonPressed & displayFirst & !gpsPositionFix) // GPS is searching (starup screen shown)
        {
            #ifdef DEBUGFLOW
                Serial.println("L - GPS");
            #endif
            // All programm logic that should run after button press
            // Check if GPS is fixed
            if (gps.date.year() > 2021)
            {
                // only this can indicate, if the date is more or less updated. tends to be 2020/1970 if old
                #ifdef DEBUGGPS
                    // char sz[32];
                    // sprintf(sz, "%02d.%02d.%02d ", gps.date.day(), gps.date.month(), gps.date.year());
                    Serial.print("Date-year: ");
                    // Serial.println(sz);
                    Serial.println(gps.date.year());
                    
                    // Serial.print("Loc: ");
                    // Serial.print(gps.location.lat());
                    // Serial.print(" ");
                    // Serial.println(gps.location.lng());
                    // Serial.print("Dist m:");

                    // Serial.print("hdop ");
                    // Serial.println(gps.hdop.hdop());

                    // Serial.print("sats ");
                    // Serial.println(gps.satellites.value());
                #endif

                if ((gps.location.isValid()) & (gps.location.age() < 2000)) // check if gps location is ok and not too old
                {
                    // Calculate the distance to the Target
                    unsigned long distanceMToTarget =
                        (unsigned long)TinyGPSPlus::distanceBetween(
                            gps.location.lat(),
                            gps.location.lng(),
                            Game_Info::TARGET_LAT,
                            Game_Info::TARGET_LNG);
                    #ifdef DEBUG
                        Serial.print("Loc: ");
                        Serial.print(gps.location.lat());
                        Serial.print(" ");
                        Serial.println(gps.location.lng());
                        Serial.print("Dist m:");

                        Serial.println(distanceMToTarget);
                        Serial.println("GPS valid");
                        Serial.println(gps.hdop.hdop());
                    #endif

                    gpsPositionFix = true; // lock the position

                    // Update EEPROM, this was an attempt
                    int numberAttempts;
                    EEPROM.get(MCU_Info::attemptAdress, numberAttempts); // get current number of attempts
                    numberAttempts += 1;                                 // Substract an attempt
                    EEPROM.put(MCU_Info::attemptAdress, numberAttempts); // Write the new number of attempts to EEPROM

                    // digitalWrite(MCU_Info::GPSPowerPIN, LOW); // Switch off the GPS before writing display & unlocking, final write, so never switch on again


                    if (distanceMToTarget < Game_Info::TARGET_DISTANCE_GOAL_M)
                    {
                        // Game is won!
                        #ifdef DEBUG
                            Serial.println("Solved");
                        #endif

                        EEPROM.put(MCU_Info::gameStateAdress, true); // Write the Game State WON to EEPROM



                        // Open the box
                        digitalWrite(MCU_Info::lockPIN, HIGH);
                        delay(MCU_Info::unlockTime);
                        digitalWrite(MCU_Info::lockPIN, LOW);

                        // Write Won Display
                        display.firstPage();
                        do
                        {
                            display.fillScreen(GxEPD_WHITE);
                            display.setTextColor(GxEPD_BLACK);
                            display.setTextSize(2);

                            display.setCursor(20, 20);
                            display.print("Geschafft!");
                            // display.setCursor(20, 40);
                            // display.print("Entspannt in der");
                            // display.setCursor(20, 60);
                            // display.print("Therme und besucht");
                            // display.setCursor(20, 80);
                            // display.print("uns mal.");
                            // display.setCursor(20, 120);
                            // display.print("Dennis");
                            // display.setCursor(20, 140);
                            // display.print("Malin, Richard");
                            // display.setCursor(20, 160);
                            // display.print("Michi, Pia");

                        } while (display.nextPage());
                        display.hibernate();
                    }
                    else 
                    {
                        // Game is not won, an attempt is spend!

                        // Show the distance screen
                        // digitalWrite(MCU_Info::GPSPowerPIN, LOW); // Switch off the GPS before writing display, final write, so never switch on again
                        display.firstPage();
                        do
                        {

                            display.fillScreen(GxEPD_WHITE);
                            display.setTextColor(GxEPD_BLACK);
                            display.setTextSize(2);

                            display.setCursor(20, 20);
                            display.print("Netter Versuch!");
                            display.setCursor(20, 40);
                            display.print("Es sind aber");
                            display.setCursor(20, 60);
                            display.print("noch");
                            display.setCursor(20, 85);
                            display.setTextSize(3);
                            display.setTextColor(GxEPD_RED);
                            display.print(distanceMToTarget / 1000);
                            display.setCursor(100,85);
                            display.setTextColor(GxEPD_BLACK);
                            display.print("km");
                            display.setTextSize(2);
                            display.setCursor(20, 110);
                            display.print("bis zum Ziel.");

                            display.setCursor(20, 150);
                            display.print("Noch ");
                            display.setCursor(90, 150);
                            display.setTextColor(GxEPD_RED);
                            display.print(Game_Info::numberSolveAttempt - (numberAttempts % Game_Info::numberSolveAttempt));
                            display.setTextColor(GxEPD_BLACK);
                            display.setCursor(20, 170);
                            if (Game_Info::numberSolveAttempt - (numberAttempts % Game_Info::numberSolveAttempt) == 1)
                            {
                                display.print("Versuch.");
                            }
                            else
                            {
                                display.print("Versuche.");
                            }

                        } while (display.nextPage());
                        display.hibernate();
                    }

                    // END of the activation:
                    #ifdef DEBUGFLOW
                        Serial.println("Reset vars");
                    #endif
                    buttonPressed = false;
                    displayFirst = false;
                    gpsPositionFix = false;

                    #ifdef DEBUGDISPLAY
                        Serial.println("start disp timer");
                    #endif
                    
                    delay(MCU_Info::delayStartDisplay); // Delay until showing start display
                    showStartDisplay();                 // show start Display

                    
                }
            }
    
            // Check if Device was on too long. No GPS fix will be archieved. Show unsuccessfull display, Switch off
            if (lastMCUTick-ButtonPressTick > MCU_Info::GPSAbortTime)
            {
                #ifdef DEBUGGPS
                    Serial.println("GPS timeout.");
                #endif
                                
                int numberAttempts;
                EEPROM.get(MCU_Info::attemptAdress, numberAttempts); // get current number of attempts
                display.firstPage();
                do
                {

                    display.fillScreen(GxEPD_WHITE);
                    display.setTextColor(GxEPD_BLACK);
                    display.setTextSize(2);

                    display.setCursor(20, 20);
                    display.print("Warum geht ihr");
                    display.setCursor(20, 50);
                    display.print("zum Lesen");
                    display.setCursor(20, 80);
                    display.print("nicht raus?");

                    
                    display.setCursor(20, 150);
                    display.print("Noch ");
                    display.setCursor(90, 150);
                    display.setTextColor(GxEPD_RED);
                    display.print(Game_Info::numberSolveAttempt - (numberAttempts % Game_Info::numberSolveAttempt));
                    display.setTextColor(GxEPD_BLACK);
                    display.setCursor(20, 170);
                    if (Game_Info::numberSolveAttempt - (numberAttempts % Game_Info::numberSolveAttempt)==1)
                    {
                        display.print("Versuch.");
                    }
                    else
                    {
                        display.print("Versuche.");
                    }
                        

                } while (display.nextPage());
                display.hibernate();

                #ifdef DEBUGDISPLAY
                    Serial.println("Start timer for showing goal display");
                #endif
                delay(MCU_Info::delayStartDisplay); // Delay until showing start display
                #ifdef DEBUGDISPLAY
                    Serial.println("Show start display again");
                #endif
                showStartDisplay(); // show start Display

                // END of the activation:
                #ifdef DEBUGFLOW
                    Serial.println("Reset variables");
                #endif
                buttonPressed = false;
                displayFirst = false;
                gpsPositionFix = false;
            }
        
        }
    }
        

    // Not on tick - Update GPS
    if(gpsSerial.available()) // run only, if GPS is in active state
    {
        // #ifdef DEBUGGPS
        //     Serial.println("GPS update loop"); //See if GPS data is read.
        // #endif
        if (gps.encode(gpsSerial.read()))
        {
            //setMCUTime(); // Update time
            #ifdef DEBUGGPS
                Serial.println("GPS active"); //See if GPS data is read.
            #endif
        }
    }

    // Check if button is pressed
    // read the state of the pushbutton value:
    if(!buttonPressed)
    {
        if(digitalRead(MCU_Info::buttonPIN))
        {   
            #ifdef DEBUGFLOW
            Serial.println("Button press");
            #endif
            buttonPressed=true;
            ButtonPressTick=millis();
        }
        
    }
}

// Set MCU time function. This will set the time, correcting for the correct timezone.
// void setMCUTime()
// {
//     int Year = gps.date.year();
//     byte Month = gps.date.month();
//     byte Day = gps.date.day();
//     byte Hour = gps.time.hour();
//     byte Minute = gps.time.minute();
//     byte Second = gps.time.second();
//     // Set Time from GPS data string
//     setTime(Hour, Minute, Second, Day, Month, Year); // set the time of the microcontroller to the UTC time from the GPS
// }

void showStartDisplay()
{
    #ifdef DEBUGDISPLAY
        Serial.println("Show Start Display function");
    #endif
    display.firstPage();
    do
    {
        display.fillScreen(GxEPD_WHITE);

        display.drawInvertedBitmap(0,0,epd_bitmap_initial_red,200,200,GxEPD_RED);

    } while (display.nextPage());
    display.hibernate();
}