#pragma once
#include <header.h>

void setup()
{
    // Setup Button PIN
    pinMode(MCU_Info::buttonPIN, INPUT);

    // Activate Serial if Debug flag defined
    #ifdef DEBUG
        Serial.begin(115200);
        Serial.println("Debug mode enabled.");
    #endif

    // Setup the Lock and pull PIN low.
    pinMode(MCU_Info::lockPIN, OUTPUT);   // Set Lock PIN to output mode
    digitalWrite(MCU_Info::lockPIN, LOW); // To open the lock use: digitalWrite(MCU_Info::lockPIN, HIGH);delay(2000);digitalWrite(MCU_Info::lockPIN, LOW); for < 1000 ms, best is 5s to make sure, everything is safe.

    
    // Setup the GPS
    pinMode(MCU_Info::GPSPowerPIN, OUTPUT); // Set the PIN
    digitalWrite(MCU_Info::GPSPowerPIN, LOW); // Switch OFF the GPS

    digitalWrite(MCU_Info::GPSPowerPIN, HIGH); // Switch on the GPS
    gpsSerial.begin(MCU_Info::GPSBaud); // Initialize Serial. Pay attention, only works if GPS on

    // Setup the eInk display.
    display.init();
    display.setRotation(2); // roate 180 degrees

    // Get/write Information from EEPROM
    // Reset 
    #ifdef RESETEEPROM
        EEPROM.put(MCU_Info::gameStateAdress,false); // Sets the game to not solved
        EEPROM.put(MCU_Info::attemptAdress,0); // Resets the number of solve attempts on startup
        #ifdef DEBUG
            Serial.println("EEPROM written - DO NOT USE IN PRODUCTION!");
        #endif
    #endif

    // Game State
    bool gameState; // False = not solved, true = solved
    // Get the state of the Game
    EEPROM.get(MCU_Info::gameStateAdress, gameState);

    #ifdef DEBUG
        Serial.print("S - Game state: ");
        Serial.println(gameState);
        Serial.print("S - attempts: ");
        int debugNumberAttempts;
        EEPROM.get(MCU_Info::attemptAdress, debugNumberAttempts);
        Serial.println(debugNumberAttempts);
    #endif

        

    

    // Do stuff, if game was already solved.
    if (gameState)
    {
        #ifdef DEBUG
            Serial.println("S - Solved. Open.");
        #endif
    
        // Open the box
        digitalWrite(MCU_Info::lockPIN, HIGH);
        delay(MCU_Info::unlockTime);
        digitalWrite(MCU_Info::lockPIN, LOW);

        // Show some Things on display
        showStartDisplay();
        delay(1000);

        // TODO: Do some end stuff
    }

}