#include <Arduino.h>

// Hardware Definitions ePaper Display
#define SPI_EPD_CLK     18   // D22
#define SPI_EPD_MOSI    23   // D23
#define SPI_EPD_MISO    -1  // not connected
#define SPI_EPD_CS      5  // D21
#define SPI_EPD_BUSY    4  // D4
#define SPI_EPD_RST     22  // D18
#define SPI_EPD_DC      21   // D19

// eInk Display Interface //
#define ENABLE_GxEPD2_GFX 1 // If 1, more ram+flash is used to use the adafruit GFX library
// #include <GFX.h>            // very basic gfx integration
//#include <GxEPD2_BW.h>      // including both doesn't use more code or ram
#include <GxEPD2_3C.h>      // including both doesn't use more code or ram

// select the display class and display driver class in the following file (new style):
#include "GxEPD2_display_selection_new_style.h"

void setup() {
  // put your setup code here, to run once:

  Serial.begin(74880);
  Serial.println("start");

  // Setup the eInk display.
  // SPI.end(); // release standard SPI pins, e.g. SCK(18), MISO(19), MOSI(23), SS(5)
  // SPI.begin(SPI_EPD_CLK, SPI_EPD_MISO, SPI_EPD_MOSI, SPI_EPD_CS); // map and init SPI pins SCK(6), MISO(-1), MOSI(7), SS(10)
  display.init();

  // Show the Busy Display
  display.firstPage();
  do
  {
    display.fillScreen(GxEPD_WHITE);
    display.setTextColor(GxEPD_BLACK);
    display.setTextSize(2);

    display.setCursor(20,20);
    display.print("Lesezeichen");
    display.setCursor(20, 50);
    display.print("werden");
    display.setCursor(20, 80);
    display.print("gesucht!");

    display.setCursor(20, 150);
    display.print("Bitte ");
    display.setCursor(20, 170);
    display.print("warten...");

  } while (display.nextPage());
  display.hibernate();
}

void loop() {
  // put your main code here, to run repeatedly:
  delay(1000);
  Serial.println("loop");
}