# Development History

## Idea
I have seen [this webpage (Reverse Geocache Puzzle)](https://www.sundial.com/stories/reverse-geocache-puzzle/) that let the idea grow to use this kind of box as generic wedding gift.

## Building Log

### February 2022
I ordered some locks to use with the box. They operate at 3.3V but need 1.5A for under a second. They are available with and without a feedback signal.
As for this project, only a single 'open action' is needed, I hope that I can spare the feedback loop. One issue will be the sourcing of 1.5A.
- [Sary lock without feedback](https://www.aliexpress.com/item/1005002920317913.html?spm=a2g0o.order_list.0.0.67671802L4VehN)
- [Sary lock with feedback](https://www.aliexpress.com/item/1005001357276043.html?spm=a2g0o.order_list.0.0.67671802L4VehN)

This is an example drawing from their product description:
With feedback signal:
![Technical drawing Sary lock with feedback signal](technical-drawing_sary-lock-withFeedback.jpg)

Without feedback signal:
![Technical drawing Sary lock without feedback signal](technical-drawing_sary-lock-withoutFeedback.jpg)

### April 2022 1
After the locks arrived I tested them with different stationary power supplies. The results were:
- From Wemos D1 mini PIN - lol what have I thought, not enough power at all!
- From Wemos D1 mini 3.3V (supplied with 5V USB) - well, not even close
- From 5V USB directly - working, manual connection
- From 3.3 V Power supply only if enough amps

I opened one lock and the mechanism is purely mechanical.
Then there is a wire, which is connected directly to the power and is (mechanicaly) unlocking the lock, when shortened.
I suspect, this is a wire which changes its length with temperature/current.
Interesting is, that there is a switch included, that is pressed when the lock is locked.
I guess this is used for the feedback signal if connected.
In every case it disconnects the 'unlocking wire', if the lock is unlocked.
I suspect, this is a security mechanism to avoid to long power supply to the wire.

### April 2022 2
I tried to activate the lock via a MOSFET.
Therefore, I used this circuit (my first use of KiCAD ever, yay!) and a WeMos D1 mini.
The MCU will be replaced by something else (less power hungry) lateron, but for a first proof of concept it **should** work.

![Snapshot of KiCAD circuit](2022-04_MOSFET-Test-Circuit.JPG)

Well, it didn't. At this moment I used a [IRL540N n-channel MOSFET](https://datasheet4u.com/datasheet-pdf/IRF/IRL540N/pdf.php?id=500387).

Some learnings I had:
- Do not use p-channel MOSFETs for this 'simple' applications. I had some of them at stock and tried them first with a different circuit (common VCC).
- MOSFET with the same voltage for Gain (G) and Drain (D) can complicate the usage. Especially for p-channel MOSFETs.
- My circuit (+ software) is working (tested with the LED, w/o the Lock).
- As soon as connecting the lock, the LED does no longer light up, when the MOSFET is activated. Not sure why. But i used only a 1A power supply. But this was working when connecting directly to VCC+GND.
- A MOSFET is usually operated with a MOSFET switching circuit. Not sure, if I will need that.
- For driving the MOSFET with 3.3V I should use a 'logic level' MOSFET

Useful Links/Videos to MOSFETs:
- https://www.electronics-tutorials.ws/transistor/tran_7.html +
- [n-channel vs. p-channel MOSFET selection guide](https://circuitjournal.com/which-mosfet-should-you-use-with-arduino) ++
- [Youtube: N-Channel MOSFET as a Switch. Turn ON a 12V Motor with Arduino. (Step-By-Step Guide)](https://www.youtube.com/watch?v=3PkpOeHTnfo) +
- [Power Relay vs. MOSFET](https://workshoppist.com/power-relays-vs-mosfets-for-microcontrollers/) ++
- [Different Logic Level MOSFETs](https://workshoppist.com/mosfets-for-arduino/) ++

### May 2022 1
Today I drew the current circuit in KiCAD (see above) and thought about the next steps. As this has to be battery operated there are two next steps:
1. Try to switch the lock on battery power. I will go for 3 AA batteries first. If needed, one could use a capacitor additionally to source enough current.
2. Look into the MOSFET issue. Why is it not working with MOSFET, but with direct power supply?

### May 2022 2
Today I made some measurements. Long story short:

I can operate my [IRL540N n-channel MOSFET](https://datasheet4u.com/datasheet-pdf/IRF/IRL540N/pdf.php?id=500387) with 5V Gate voltage, but not with 3.3V. So this could be just the wrong MOSFET. Bought some expensive (1€ per part) MOSFETS from [this article](https://workshoppist.com/mosfets-for-arduino/#bestfets): PSMN1R1-30PL and PSMN2R5-60PL, waiting for delivery.
In the mean time, I can check the possible operation with AA batteries and develop the GPS/display functionality.
