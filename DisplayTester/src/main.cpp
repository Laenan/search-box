// Debug Flag, if defined, Serial will output informations
#define DEBUG

// Should be commented out for production! Only use once, to reset the EEPROM
//#define RESETEEPROM

#include "header.h" // Import the header

#include <Fonts\FreeSans12pt7b.h>
#include "bitmap_initial.h"
void drawBitmapPairs(const unsigned char *bitmap_pair);

#include "setup.h" // Import setup function.

    int numberTrail = 5;
int totalTrail = 10;
int distance = 48305;

void loop()
{
    display.firstPage();
    do
    {
        display.fillScreen(GxEPD_WHITE);

        //display.drawInvertedBitmap(0, 0, epd_bitmap_inital_black, 200, 200, GxEPD_BLACK);
        display.drawInvertedBitmap(0, 0, epd_bitmap_initial_red, 200, 200, GxEPD_RED);

    } while (display.nextPage());
    display.hibernate();

    delay(10000);
    // display.clearScreen();

    // // Show the Busy Display
    // display.firstPage();
    // do
    // {
    //     display.fillScreen(GxEPD_WHITE);

    //     display.setTextColor(GxEPD_BLACK);
    //     display.setTextSize(1);
    //     display.setCursor(20,10);
    //     display.setFont(&FreeSans12pt7b);
    //     display.print("Starte Hochzeitsbuch...");

    // //     display.fillRect(0, 70, 200, 60, GxEPD_COLORED);
    // //     display.setTextColor(GxEPD_WHITE);
    // //     display.setTextSize(1);

    // // #ifdef CUSTOMFONT
    // //     display.setFont(&FreeMono12pt7b);
    // // #endif
    // //     display.setCursor(20, 90);
    // //     display.print("Startup - Busy.");

    // } while (display.nextPage());
    // display.hibernate();

    // delay(10000);
    }

