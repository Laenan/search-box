#pragma once
#include <Arduino.h>
struct MCU_Info {

    // Tick interval
    static constexpr long long MCUTick = 1000; // in ms

    //GPS
    static constexpr int GPSPowerPIN = 2; // MOSFET for turning GPS on/off
    static constexpr int RXPin = 4, TXPin = 5;
    static constexpr uint32_t GPSBaud = 9600; // Default baud of NEO-6M is 9600
    static constexpr long long GPSAbortTime = 600000; // in ms, time until abort activation

    // POWER CONTROL
    // Define Power Switch PIN
    static constexpr int powerPIN = 6; // Digital 6 PIN, Connected to n-MOSFET. Will Power OFF circuit.

    // LOCK CONTROL
    // Define Lock PIN
    static constexpr int lockPIN = 3; // Digital PIN 3, connected to MOSFET ground. Will unlock the lock
    static constexpr int unlockTime = 3000; // Time in ms to unlock the lock

    // EEPROM
    static constexpr int attemptAdress = 20; // Adress of Number Attempts left
    static constexpr int gameStateAdress = 999; // Adress of GameState
};