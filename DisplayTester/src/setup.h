#pragma once
#include <header.h>

void setup()
{
    // Activate Serial if Debug flag defined
    #ifdef DEBUG
        Serial.begin(115200);
        Serial.println("Debug mode enabled.");
    #endif

   
    // Setup the eInk display.
    display.init();
}