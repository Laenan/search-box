#pragma once
#include <header.h>

#define DEBUGGPS
#define DEBUG

void setup()
{
    // Switch PowerPIN on (switching it LOW, will turn of the MCU)
    // pinMode(MCU_Info::powerPIN, OUTPUT);
    // digitalWrite(MCU_Info::powerPIN, HIGH);

    // Activate Serial if Debug flag defined
    #ifdef DEBUGGPS
        Serial.begin(74880);
        Serial.println("Debug mode enabled.");
    #endif

    // Setup the Lock and pull PIN low.
    pinMode(MCU_Info::lockPIN, OUTPUT);   // Set Lock PIN to output mode
    digitalWrite(MCU_Info::lockPIN, LOW); // To open the lock use: digitalWrite(MCU_Info::lockPIN, HIGH);delay(2000);digitalWrite(MCU_Info::lockPIN, LOW); for < 1000 ms, best is 5s to make sure, everything is safe.

    // Setup the eInk display.
    display.init();
    display.setRotation(2); // roate 180 degrees

    // Get/write Information from EEPROM
    // Reset 
    #ifdef RESETEEPROM
        EEPROM.put(MCU_Info::gameStateAdress,false); // Sets the game to not solved
        EEPROM.put(MCU_Info::attemptAdress,0); // Resets the number of solve attempts on startup
        #ifdef DEBUG
            Serial.println("EEPROM written - DO NOT USE IN PRODUCTION!");
        #endif
    #endif

    // Game State
    bool gameState; // False = not solved, true = solved
    // Get the state of the Game
    EEPROM.get(MCU_Info::gameStateAdress, gameState);

    #ifdef DEBUG
        Serial.print("SETUP - Game state read from EEPROM: ");
        Serial.println(gameState);
        Serial.print("SETUP - Number of performed attempts: ");
        int debugNumberAttempts;
        EEPROM.get(MCU_Info::attemptAdress, debugNumberAttempts);
        Serial.println(debugNumberAttempts);
    #endif

    

    // Do stuff, if game was already solved.
    if (gameState)
    {
        #ifdef DEBUG
            Serial.println("Box was already solved, open in Setup Loop.");
        #endif
    
        // Open the box
        digitalWrite(MCU_Info::lockPIN, HIGH);
        delay(MCU_Info::unlockTime);
        digitalWrite(MCU_Info::lockPIN, LOW);

        // Show some Things on display
        showStartDisplay();
        delay(1000);

        // Power Off the box (if display ready)
        // if (true)
        // {
        //     digitalWrite(MCU_Info::powerPIN, LOW);
        // }
    }

    // Show the Busy Display
    display.firstPage();
    do
    {
        display.fillScreen(GxEPD_WHITE);
        display.setTextColor(GxEPD_BLACK);
        display.setTextSize(2);

        display.setCursor(20,20);
        display.print("Lesezeichen");
        display.setCursor(20, 50);
        display.print("werden");
        display.setCursor(20, 80);
        display.print("gesucht!");

        display.setCursor(20, 150);
        display.print("Bitte ");
        display.setCursor(20, 170);
        display.print("warten...");

    } while (display.nextPage());
    display.hibernate();

    // Setup the GPS
    // pinMode(MCU_Info::GPSPowerPIN, OUTPUT); // Set the PIN
    // digitalWrite(MCU_Info::GPSPowerPIN, HIGH); // Switch on the GPS

    gpsSerial.begin(MCU_Info::GPSBaud); // Initialize Serial. Pay attention, only works if GPS on
}