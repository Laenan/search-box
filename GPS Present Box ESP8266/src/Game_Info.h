#pragma once
#include <Arduino.h>

struct Game_Info {
// GPS Target Information:
static constexpr double TARGET_LAT = 47.907428, 
                        TARGET_LNG = 8.159647; // Current Target: Tittisee

// static constexpr double TARGET_LAT = 51.224660,
//                         TARGET_LNG = 6.792793; // Test Target: Astor

static constexpr double TARGET_DISTANCE_GOAL_M = 5000; // Distance to target, where Box is opened in meter

// GAME INPUT
static constexpr int numberSolveAttempt = 10; // Number of attempts (with GPS fix)
};