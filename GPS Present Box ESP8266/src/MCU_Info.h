#pragma once
#include <Arduino.h>

// Hardware Definitions ePaper Display
#define SPI_EPD_CLK     14   // D5
#define SPI_EPD_MOSI    13   // D7
#define SPI_EPD_MISO    -1  // not connected
#define SPI_EPD_CS      15  // D8
#define SPI_EPD_BUSY    4  // D2
#define SPI_EPD_RST     2  // D4
#define SPI_EPD_DC      0   // D3

struct MCU_Info {

    // Tick interval
    static constexpr long long MCUTick = 1000; // in ms

    //GPS
    static constexpr int GPSPowerPIN = -99; // MOSFET for turning GPS on/off
    static constexpr int RXPin = 3, TXPin = 1;
    static constexpr uint32_t GPSBaud = 9600; // Default baud of NEO-6M is 9600
    static constexpr long long GPSAbortTime = 300000; // in ms, time until abort activation

    // POWER CONTROL
    // Define Power Switch PIN
    static constexpr int powerPIN = -99; // Digital 6 PIN, Connected to n-MOSFET. Will Power OFF circuit.

    // Button
    static constexpr int buttonPIN = 16; // Button connected

    // LOCK CONTROL
    // Define Lock PIN
    static constexpr int lockPIN = 5; // Digital PIN 3, connected to MOSFET ground. Will unlock the lock
    static constexpr int unlockTime = 3000; // Time in ms to unlock the lock

    // EEPROM
    static constexpr int attemptAdress = 20; // Adress of Number Attempts left
    static constexpr int gameStateAdress = 999; // Adress of GameState

    // Waiting periods
    static constexpr int delayStartDisplay = 10000; //us 45s delay (+ display update time) for seeing the info screen
};